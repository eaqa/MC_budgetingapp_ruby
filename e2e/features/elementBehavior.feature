Feature: Category #3: Interface element behavior
  In order to get value from the Budget app
  Front end users should be able to
  get consistent feedback from system and user input

  Background:
    When I visit the budget app
    Then the Budget tab is loaded

  Scenario: 3.1 - The main view of the Budget app is reset to the default view upon manual browser refresh
    Given I have entered an entry to the Budget app
    |category|description|value|
    |Entertainment  |Book - Dune|10.00|

    And I refresh the browser
    Then my entry is removed and the app is reset to the default view


  Scenario: 3.4 - category items that subtract from the user's budget are correctly reflected within summing section of the Budget app.
    Given I have entered an entry to the Budget app
    |category|description|value|
    | Car   | Bugatti Veyron | 2213.93 |
    And I have entered an entry to the Budget app
    |category|description|value|
    | Misc   | Popcorn | 1.00 |
    Then my ending ballence is corrently reflected in "Working Ballance"



