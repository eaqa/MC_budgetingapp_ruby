require 'capybara'
require 'capybara/dsl'
require 'capybara/cucumber'
require 'capybara/rspec'
require 'cucumber'
require 'rspec'
require 'site_prism'
require 'selenium-webdriver'
require 'pry'

Dir[File.dirname(__FILE__) + '/../../page_objects/*.rb'].each { |file|
  require file
}

#configure Chrome
Capybara.configure do |config|
  config.run_server = false
  config.default_driver = :selenium_chrome
  config.default_selector = :xpath
  Capybara.app_host = "http://localhost:3000"
  Capybara.server_host = "localhost"
  Capybara.server_port = "3000"
end

# Register Chrome
Capybara.register_driver :selenium_chrome do |app|
  Capybara::Selenium::Driver.new(app, :browser => :chrome)
end

# #defining new chrome session to go to localhost:3000
# session = Capybara::Session.new :selenium_chrome
# session.visit 'http://localhost:3000'

#allowing for insecure content on localhost
options = Selenium::WebDriver::Chrome::Options.new
options.add_argument('--user-data-dir=/tmp/unsafe --unsafely-treat-insecure-origin-as-secure=http://localhost')


