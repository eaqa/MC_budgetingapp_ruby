And(/^I refresh the browser$/) do
  @app.budget.load
end

Then(/^my entry is removed and the app is reset to the default view$/) do
  x = @app.budget.all_there?

  if x == true
    puts "Bravo: All Default Budget Page Elements are present"
  else
    puts "ERROR: Missing Page Elements"
  end

  expect(@app.budget).to be_all_there
end


Then(/^my ending ballence is corrently reflected in "([^"]*)"$/) do |arg|
  working_bal = @app.budget.sumWorkingBal.text
  expect(working_bal).to eq('-$1.00')
end
