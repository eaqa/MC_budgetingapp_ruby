require 'twitter_cldr'

Given(/^I have entered an entry to the Budget app$/) do |table|
  data = table.transpose.raw.inject({}) do |hash, column|
    column.reject!(&:empty?)
    hash[column.shift] = column
    hash
  end
  cat = p data['category']
  des = p data['description']
  val =p data['value']

  catValue =  cat.fetch(0)
  desValue = des.fetch(0)
  valValue = val.fetch(0)

  valValue_i = valValue.to_i
  @app.budget.sumCatDropdown.select(catValue)
  @app.budget.sumDescField.set(desValue)
  @app.budget.sumValueField.set(valValue)
  @app.budget.sumAddButton.click
  expect(page).to have_content(catValue)
  expect(page).to have_content(desValue)
 # binding.pry

=begin
  todo work on expect:
valValue_i passes this assertion for all test items
besides for:
    |category|description|value|
    | Car   | Bugatti Veyron | 2213.93 |

expected to find text "-$2,213.00"
actual "n-$2,213.93"
not sure where this 'n' is coming form?
=end

 #expect(page).to have_content("-"+valValue_i.localize(:us).to_currency.to_s)
end
