When(/^I visit the budget app$/) do
  @app.budget.load
end

Then(/^the Budget tab is loaded$/) do
  @app.budget.displayed?
end


When(/^I click the Reports Navigation Button$/) do
  @app.budget.ReportsBtn.click
end

Then(/^I am taken to the Reports page$/) do
  expect(@app.budget.current_url).to include("/reports")
end

And(/^I click the Budget Navigation Button$/) do
  @app.budget.ReportsBtn.click
end

Then(/^I am taken to the Budget page$/) do
  @app.budget.displayed?
end
