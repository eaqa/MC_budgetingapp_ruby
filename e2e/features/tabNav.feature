Feature: Category #1: User Navigation
  In order to see both the Budget and Reports views
  Front end users should be able to
  Navigate between the two views via button navigation

  Scenario: 1.1 - Default view of the Budget app is the Budget Tab
    When I visit the budget app
    Then the Budget tab is loaded

  Scenario: 1.3 - User can navigate between the Budget tab and the Reports tab by clicking on the respective navigation buttons
    Given I visit the budget app
    When I click the Reports Navigation Button
    Then I am taken to the Reports page
    And I click the Budget Navigation Button
    Then I am taken to the Budget page








