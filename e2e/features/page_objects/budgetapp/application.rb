module Budgetapp
  module PageObjects
    class Application
      def initialize
        @pages = {}
      end

      def budget
        @pages[:budget] ||= Budgetapp::PageObjects::Pages::BudgetPage.new
      end

      def  reports
        @pages[:reports] ||= Budgetapp::PageObjects::Pages::ReportsPage.new
      end

    end
  end
end
