module Budgetapp
  module PageObjects
    module Pages
      class BudgetPage < SitePrism::Page
        set_url '/budget'
        set_url_matcher /budget/

        #nav buttons
        element :budgetBtn, "//*[@id='root']/main/div/a[1]"
        element :ReportsBtn, "//*[@id='root']/main/div/a[2]"

        #default items
        element :dGroceries, "//*[@id='root']/main/section/table/tbody/tr[1]"
        element :dTravel, "//*[@id='root']/main/section/table/tbody/tr[2]"
        element :dIncome, "//*[@id='root']/main/section/table/tbody/tr[3]"
        element :dEntertainment, "//*[@id='root']/main/section/table/tbody/tr[4]"
        element :dMisc, "//*[@id='root']/main/section/table/tbody/tr[5]"
        element :dIncome, "//*[@id='root']/main/section/table/tbody/tr[6]"

        #budget summing section
        element :sumContainer, "//*[@id='root']/main/section/table/tfoot/tr"
        element :sumWorkingBal, "//*[@id='root']/main/section/div/div/div[5]/div/div[1]"
        #cat dropdown
        element :sumCatDropdown,".//select[@name='categoryId']"

        #description field
        element :sumDescField, ".//input[@name='description']"

        #value field
        element :sumValueField, ".//input[@name='value']"

        #Add button
        element :sumAddButton, ".//button[@type='submit']"
        end

      end
    end
  end

