<!-- TOC -->

- [Test Plan](#test-plan)
        - [Objective](#objective)
- [Test Scenarios](#test-scenarios)
        - [Catergory #1](#catergory-1)
        - [Catergory #2](#catergory-2)
        - [Catergory #3](#catergory-3)
- [Automation Task](#automation-task)
- [Setup and Background](#setup-and-background)
        - [Prelilmaniary Tasks](#prelilmaniary-tasks)
        - [To run features](#to-run-features)
        - [Tech Used](#tech-used)

<!-- /TOC -->

# Test Plan

### Objective

**Catergory #1: User Navigation**

In Navigation verification, I will test that the front-end user can travel between the application's different views and is presented with the correct view at the right time.

**Catergory #2: User Interaction**

Front-end User is able to interact with all of the application's interface elements as expected.

**Catergory #3: Interface element behavior:**

Dropdown, Textfield, button elements respond in a constent way in response to system and Front-end User interaction. This also includes display of dynamic data, outputted from the system - in the form of numbers and graph elements.

# Test Scenarios

### Catergory #1 

* 1.1 - When user loads the URL for the budgget app, the *default view* presented to the user is the "Budget" Tab.
* 1.2 - When the user is already within the budget app (regardless of which view they are on) upon a manual browser refresh the user is prested the *default view* of the app.
* 1.3 - User can navigate between the *Budget* tab and the *Reports* tab by clicking on the respective navigation buttons and the respective URLs are upadated as indicated below:
* The *Budget Tab* has the url of ```~/budget```
* The *Reprots Tab* has the url of ```~/reports/inflow-outflow```
* 1.4 - User is able to switch between the Reproting *Inflow vs Outflow* and *Spending by Category* views by the respective buttons and the respective URLs are updated as indicated below:
* *Inflow vs Outflow* has the url of ```~/reports/inflow-outflow```
* *Spending by Category* has the url of ```~/reports/spending```

### Catergory #2  

* 2.1 - *Category dropdown* - User is able to click dropdown and select from the provided list of predefined Categories items.
* 2.2 - *Description Field* - User is able to add a description of their budget item, within the Description Field.
* 2.3 - *Value Field* - User is able to add interger/numeric data to the Value field; positive and negative numbers are allowed, in that the user can append a + or - sign to their added interger within the Value field.
* 2.4 - *Add Button* - Default behavior of button is that it is grayed out/not interactible until a valid input is added to the Value Field.

### Catergory #3  

* 3.1 - Upon manual browser refresh the items listed in the *Budget* main view is reset to only show the default list of items and the user's entered items are removed.
* 3.2 - User's added values to the add item interface (cat dropdown, description field, value field) are added to the main view of the *Budget* main view.
* 3.3 - Only *Income* category item adds to the user's budget; all other category options subtract from the user's budget.
* 3.4 - category items that subtract from the user's budget are correctly reflected within summing section of the Budget app.
* 3.5 - category items that add to the user's budget are correctly reflected within summing section of the Budget app.
* 3.6 - A newly added category item added to the *Budget* main view is also added to the *Spending by Category* view.
* 3.7 - Items that add to a user's budget are refelcted in the "Amount" section of the *Budget* main view as GREEN.
* 3.8 - Items that subtract from a user's budget are refelcted in the "Amount" section of the *Budget* main view as RED.

# Automation Task  

The following Test Scenarios are going to be automated for this coding exercise:

|Scenario number|expected to pass/fail|
|---------------|---------------------|
|TS 1.1         |PASS                 |
|TS 1.3         |PASS                 |
|TS 3.1         |PASS                 |
|TS 3.4         |FAIL                 |

TS 3.4 fails, when the user does the following:
1. User visits the Budget app for the first time 
2. User reduces their 'working balance to Zero Dollars
* working balance = $0.00
3. User adds an item (eg 'car') that subtracts from their income 
* Car ammount = $1.00
* Car item is added to the Budget List as: -$1.00
4. The system responds by adding to their income by the amount entered in step 3  
* working balance = $1.00  

**Expectation:**  
The Working balance would go into the negatives. In above example, the WB would = -$1.00  
**Actual:**  
The system instead adds to their working balaence. In above example, the WB would = $1.00

It seems that the system does not allow for the working balance to go in the negatives but does not account for this correctly.

# Setup and Background  

### Prelilmaniary Tasks  

To make sure your system is setup correctly, please conduct the following:  

Please ensure that you have the latest [Chromedriver](https://sites.google.com/a/chromium.org/chromedriver/downloads) installed and setup...

On my system I use npm [selenium-standalone](https://www.npmjs.com/package/selenium-standalone) 

1. cd into ```e2e``` directory
2. rvm use ruby-2.3.0
3. rvm gemset create MC_e2e
4.  rvm gemset use MC_e2e
5. gem install bundler
6. bundle install

After cloning my repo, you may have to within the app directory - build the app with NPM and start the app server.
Here is a [print out log](https://docs.google.com/document/d/1C4y_5u0y5XgQUtk5KR5209sc4Jdo7_VaWntxgxZoz74/edit?usp=sharing) of the commands I had to run within the app directory to get the app to run on localhost:3000

 ### Gitlab repo for tests  
https://gitlab.com/eaqa/MC_budgetingapp_ruby

All test files are found in the ```e2e``` directory 
Two *cucumber* feature tests:
1. features/elementBehavior.feature
2. features/tabNav.feature 

### To run features  
Navigate to the ```e2e``` directory
   - ```rake elementBehavior_chrome``` - to run the features/elementBehavior.feature 
    - ```rake tabNav_chrome``` - to run the features/tabNav.feature
    - ```rake all_chrome``` - to run both features


### Tech Used  

- Ruby-2.3.0  
- RVM
- Bundler
- Automation Framework:  
    * Capybara  
    * Cucumber
- IDEs used:  
    * RubyMine
    * VScode
- Git branching model: [Gitflow](https://nvie.com/posts/a-successful-git-branching-model/)
- Generated HTML report of the test results
    * found at ```e2e/reports/test_results.html```
- Page Object Pattern used:
    * utalized [Siteprism](https://github.com/natritmeyer/site_prism)  
- Rake Tasks employed: 
    - ```rake elementBehavior_chrome``` - to run the features/elementBehavior.feature  
    - ```rake tabNav_chrome``` - to run the features/tabNav.feature
    - ```rake all_chrome``` - to run both features
